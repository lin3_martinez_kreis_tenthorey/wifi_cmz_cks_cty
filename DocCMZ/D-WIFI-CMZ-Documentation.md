---
title: WIFI - Documentation
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\wifijpg)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

# Mise en place

## Création du certificat machine

Il faut ouvrir la console MMC Certificate Templates puis cliquer-droit sur Workstation Authentification et sélectionner Duplicate Template.

ENsuite, il faut cliquer-droit et sélectionner Properties.

Dans la fenêtre qui s'ouvre, il faut cliquer sur l'onglet Security, cliquer sur Authenticated Users, cocher la case à cocher Autoenroll et cliquer sur le bouton Apply.

## Création de la GPO Autoenroll

Afin de créer la GPO qui assigne les certificats machine, il faut ouvrir l'outil de gestion des stratégies de groupe.

Puis, il faut cliquer-droit sur le nom du domaine et sélectionner Create a GPO in this domain and Link it here.

Après cela, il faut cliquer-droit et sélectionner Edit.

Puis, il faut cliquer sur Computer Settings->Policies->Windows Settings->Security Settings.->Public Key Policies.

Ensuite, il faut cliquer sur Certificate Services Client - Auto-Enrollment Settings.

Dans la fenêtre qui s'ouvre il faut sélectionner Enabled dans le champ Configuration Model, cocher Renew expired certificates et Update certificates that use certificate templates.

Puis, il faut cliquer sur le bouton OK.

Ensuite, il faut cliquer sur le bouton Add situé en dessous de Security Filtering.

Puis, il faut saisir le nom du groupe désirée et cliquer sur le bouton OK.

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://docs.microsoft.com/en-us/windows-server/networking/core-network-guide/cncg/server-certs/configure-server-certificate-autoenrollment

https://www.ntweekly.com/2017/12/21/certificate-auto-enrollment-using-group-policy-windows-server-2016-ca/

\newpage

# Annexes

Ment.
